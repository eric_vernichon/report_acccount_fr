##############################################################################
#
# Copyright (c) 2007-2013  Eric Vernichon <eric@vernichon.fr> All Rights Reserved.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsability of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# garantees and support are strongly adviced to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from osv.orm import TransientModel, except_orm
from osv import fields
from tools.translate import _
import logging

class balance_sheet(TransientModel):
	_name = 'balance.sheet'
	_description = 'Balance Sheet'
	_columns = {         
                'fiscalyear_id':fields.many2one('account.fiscalyear','FiscalYear'),
                'period_ids': fields.many2many('account.period','','','','Periodes'),
    }
	
	def default_get(self, cr, uid, fields_list, context=None):
		fiscalyear_obj = self.pool.get('account.fiscalyear')
		res = {}
		res['fiscalyear_id'] = fiscalyear_obj.find(cr, uid)
		return res	 
	
	def launch_report(self, cr, uid, ids, context=None):
		res =  self.read(cr,uid,ids)[0]
		datas = {
			 'ids': context.get('active_ids',[]),
			 'model': 'report.account.fr.lignes',
			 'form': res
				 }
		return {
			'type': 'ir.actions.report.xml',
			'report_name': 'report.account.fr.bilan',
			'datas': datas,
	}
balance_sheet()