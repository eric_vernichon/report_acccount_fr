# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright (c) 2007-2013  Eric Vernichon <eric@vernichon.fr> All Rights Reserved.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsability of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# garantees and support are strongly adviced to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

import time
from report import report_sxw
from resultat import resultat
from rml_parse import report_fr_rml_parse
from formatage import formate
class bilan(report_fr_rml_parse):
    def __init__(self, cr, uid, name, context):
        super(bilan, self).__init__(cr, uid, name, context)
        self.localcontext.update( {
            'time': time,
            'annee': self._annee,
            'chargement': self.chargement,
            'set_variable': self.set_variable,
            'formate': formate,
            'resultat': resultat(cr,uid,name,context)
        })
        self.context = context
        
    def chargement(self,form):
            requete = "select name,definition,exceptions,valeur,type,somme from report_account_fr_parametres where somme = 'f' and report = 'bilan'"
            self.cr.execute(requete)
            resultats =self.cr.dictfetchall()
            for champresultat in resultats:
                self.donnees(form,eval(champresultat['definition']),champresultat['name'],eval(champresultat['exceptions']),champresultat['type'],champresultat['valeur'])
            requete = "select name,definition,exceptions,valeur,type,somme from report_account_fr_parametres where somme = 't' and report = 'bilan' order by name"
            self.cr.execute(requete)
            resultats =self.cr.dictfetchall()
            for champresultat in resultats:
                variable=champresultat['name']
                res =self.somme(eval(champresultat['definition']))
                self.set_variable(variable,res)
                

            
    def _annee(self,form):
        fy_id  = form['fiscalyear_id'][0]
        return self.pool.get('account.fiscalyear').read(self.cr, self.uid, fy_id )['date_start'][:4]
    
    def set_variable(self,variable,valeur):
        self.localcontext.update({variable:valeur})

    def get_variable(self,variable):
        return self.localcontext[variable]

    def somme(self,variables):
        res=0
        for variable in variables:
            res=res +self.localcontext[variable]
        return int(round(res))

    # comptes : comptes a additionner
    #type : S = solde D = debit C = credit
    # variable: variable ou stoker la valeur
    #exeption : compte a exclure
    #champ  valeur  a retourner solde,debit,credit
    def donnees(self, form,comptes,variable="x",exception=[],type="S",champ="solde"):
        ctx = self.context.copy()
        fy_id  = form['fiscalyear_id'][0]
        annee = self.pool.get('account.fiscalyear').read(self.cr, self.uid,fy_id )['date_start'][:4]
        
        if len(form['period_ids']) >1:
            periode= " in "+str(tuple(x for x in  form['period_ids']))
        elif len(form['period_ids']) == 0:
            yperiode=self.pool.get('account.period').search(self.cr, self.uid,[('fiscalyear_id','=',fy_id)])
            periode= " in "+str(tuple(x for x in  yperiode))
        else:
            periode = " = "+str(form['period_ids'][0])
        
        res=0.0
        if type== "S":
            compte_recherche="("
            for compte in comptes:
                compte_recherche += "numero like '"+compte+"%'  or "
            compte_recherche = compte_recherche[:-3]+")"
            if len(exception)>0:
                compte_recherche = compte_recherche+" and ("
                for compte in exception:
                    compte_recherche += "numero not like '"+compte+"%'  and "
                compte_recherche = compte_recherche[:-4]+")"
            requete = "select sum("+champ+") as resultat  from report_account_fr_lignes where "+compte_recherche+" and period_id "+str(periode)
            self.cr.execute(requete)
            res = self.cr.dictfetchall()[0]['resultat']
        if type == "D" or type == "C":
            for compte in comptes:
                exceptions=""
                if  exception:
                    exceptions="and "
                    for compte_except in exception:
                        exceptions += "numero not like '"+compte_except+"%'  and "
                    exceptions = exceptions[:-4]
                requete = "select sum("+champ+") as resultat,numero  from report_account_fr_lignes where numero like '"+compte+"%' "+exceptions+" and period_id "+str(periode) +" group by numero"
                self.cr.execute(requete)
                resultats =self.cr.dictfetchall()
                for resultat in resultats:
                    if type == "D" and (resultat['resultat'] < 0.0):
                        res+= resultat['resultat']
                    if type == "C" and resultat['resultat'] > 0.0:
                        res+= resultat['resultat']
        if not res:
            res=0.0
        
        self.set_variable(variable, res)
        return int(round(res))

    

report_sxw.report_sxw('report.report.account.fr.bilan', 'report.account.fr.lignes','addons/report_account_fr/report/bilan.rml', parser=bilan, header=False)

