# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright (c) 2007 EVI.  All Rights Reserved.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsability of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# garantees and support are strongly adviced to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

import time
from report import report_sxw
from resultat import resultat
from rml_parse import report_fr_rml_parse

class grandlivre(report_fr_rml_parse):
    listecomptes={}
    totalcompte={}
    compteur=0
    journaux={}
    anneegl=""
    periode=""
    nomcomptes={}
    devises={}
    devisecompany=''
    
    def __init__(self, cr, uid, name, context):
        super(grandlivre, self).__init__(cr, uid, name, context)
        self.localcontext.update( {
            'time': time,
            'lignes': self.lignes,
            'debut': self._debut,
            'fin': self._fin,
            'listecomptes':self.listecomptes,
            'totalcompte':self.totalcompte,
            
        })
        self.context = context
        
    def _periode(self):
        form=self.datas['form']
        if self.periode=="":
            if len(form['periods'][0][2]) >1:
                self.periode= " in "+str(tuple(x for x in  form['periods'][0][2]))
            elif len(form['periods'][0][2]) == 0:
                yperiode=self.pool.get('account.period').search(self.cr, self.uid,[])
                self.periode= " in "+str(tuple(x for x in  yperiode))
            else:
                self.periode = " = "+str(form['periods'][0][2][0])
        return self.periode
        
    def _debut(self):
        form=self.datas['form']
        requete = "select date_start from account_period where id "+self._periode()+" order by date_start"
        self.cr.execute(requete)
        deb= self.cr.dictfetchone()
        return deb['date_start']
    
    def _fin(self):
        form=self.datas['form']
        requete = "select date_stop from account_period where id "+self._periode()+" order by date_stop desc"
        self.cr.execute(requete)
        fin= self.cr.dictfetchone()
        return fin['date_stop']
        
        
        return self.pool.get('account.fiscalyear').read(self.cr, self.uid, form['fiscalyear'])['date_stop']
    
    def nomjournal(self,journal_id):
        if not self.journaux.has_key(journal_id):
            self.journaux[journal_id]=self.pool.get('account.journal').read(self.cr, self.uid, journal_id)['code']
        return self.journaux[journal_id]
    
    def nomcompte(self,account_id):
        if not self.nomcomptes.has_key(account_id):
            self.nomcomptes[account_id]=self.pool.get('account.account').read(self.cr, self.uid, account_id)['name'] 
        return self.nomcomptes[account_id]
    
    def lettrage(self,reconcile_id):
        return self.pool.get('account.move.reconcile').read(self.cr, self.uid, reconcile_id)['name'] 
    
    def listecomptes(self):
        form=self.datas['form']
        periode=self._periode()
        if form['racine']:
            requete = "select id,code,name from account_account  where code like '"+str(form['racine'])+"%' order by code"
            self.cr.execute(requete)
            comptes = self.cr.dictfetchall()
        elif len(form['comptes'][0][2]) >1: # Plusieurs comptes selectionnes
            comptes= tuple(x for x in  form['comptes'][0][2])
            requete = "select id,code,name from account_account  where id in "+str(comptes)+" order by code"
            self.cr.execute(requete)
            comptes = self.cr.dictfetchall()
            #comptes=self.pool.get('account.account').read(self.cr, self.uid,comptes)
        elif  len(form['comptes'][0][2]) ==1: # un compte selectionne
            requete = "select id,code,name from account_account  where id ="+str(form['comptes'][0][2][0])+" order by code"
            self.cr.execute(requete)
            comptes = self.cr.dictfetchall()
            #comptes=(self.pool.get('account.account').read(self.cr, self.uid,form['comptes'][0][2][0]),)
        else: # tout les comptes
            requete = "select account_id from account_move_line  group by account_id order by account_id "
            self.cr.execute(requete)
            res = self.cr.dictfetchall()
            ids=tuple(x['account_id'] for x in  res)
            requete = "select id,code,name from account_account  where id in "+str(ids)+" order by code "
            self.cr.execute(requete)
            comptes = self.cr.dictfetchall()
        if  form['nonmouvemente']==False:
            listecomptes=[]
            for compte in comptes:
                requete = "select name from account_move_line where account_id =  "+str(compte['id'] )+" and period_id "+str(periode) +" limit 1" 
                self.cr.execute(requete)
                res = self.cr.dictfetchall()
                if len(res)>0:
                    listecomptes.append(compte)
        comptes=listecomptes
        res =tuple({'id':x['id'],'code':x['code'],'name':x['name']} for x in  comptes)
        return res
    
    def nomdevise(self,currency_id):
        if not self.devises.has_key(currency_id):
            requete="select name from res_currency where id="+str(currency_id)
            self.cr.execute(requete)
            res = self.cr.dictfetchall()
            self.devises[currency_id]=res[0]['name']
        return self.devises[currency_id]

    def lignes(self, compte):
        form=self.datas['form']
        self.compteur=self.compteur+1
        self.totalcompte['debit']=0
        self.totalcompte['credit']=0
        ctx = self.context.copy()
        annee = self.anneegl
        periode = self.periode
        if not self.devisecompany:
            requete="select currency_id from res_company;"
            self.cr.execute(requete)
            res = self.cr.dictfetchall()
            self.devisecompany=res[0]['currency_id']
        res=0.0
        if not form['lettrer']:
            requete = "select move.name as move_id,line.date,line.name,line.ref as ref,line.journal_id as journal_id, line.reconcile_id as reconcile_id, line.debit,line.credit,line.amount_currency as montantdevise,currency_id as devise  from account_move_line  line inner join account_move move on line.move_id = move.id and line.state<>'draft' and  account_id  =  "+str(compte )+" and line.period_id "+str(periode) +" order by date" 
        if form['lettrer']:
            requete = "select move.name as move_id,line.date,line.name,line.ref as ref,line.journal_id as journal_id, line.reconcile_id as reconcile_id, line.debit,line.credit,line.amount_currency as montantdevise,currency_id as devise  from account_move_line  line inner join account_move move on line.move_id = move.id and line.state<>'draft' and line.reconcile_id is  null and  account_id  =  "+str(compte )+" and line.period_id "+str(periode) +" order by date" 
        self.cr.execute(requete)
        res = self.cr.dictfetchall()
        soldeprog=0
        if len(res)>0:
            for ecriture in res:
	        
                ecriture['journal']=self.nomjournal(ecriture['journal_id'])
                if ecriture['reconcile_id']:
                    ecriture['lettrage']=self.lettrage(ecriture['reconcile_id'])
                else:
                    ecriture['lettrage']=''
                
                if ecriture['debit'] <> 0 :
                    self.totalcompte['debit']=self.totalcompte['debit']+ ecriture['debit'] 
                    ecriture['soldeprog']=soldeprog- ecriture['debit'] 
                    
                elif ecriture['credit'] <> 0 :
                    self.totalcompte['credit']=self.totalcompte['credit']+ecriture['credit']
                    ecriture['soldeprog']=soldeprog+ecriture['credit']
                else:
                    ecriture['soldeprog']=soldeprog
                if  ecriture['devise']==None:
                    ecriture['montantdevise']= 0.0 
                elif ecriture['devise']<>self.devisecompany:
                    ecriture['devise']=self.nomdevise(ecriture['devise'])
                else:                
                    ecriture['montantdevise']= 0.0                                                                                                                                                                              
                soldeprog=ecriture['soldeprog']
        
        return (res)
    

report_sxw.report_sxw('report.report.account.fr.grandlivre', 'account.account','addons/report_account_fr/report/grandlivre.rml', parser=grandlivre,header=True)

