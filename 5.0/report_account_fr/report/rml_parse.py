# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright (c) 2007 EVI.  All Rights Reserved.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsability of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# garantees and support are strongly adviced to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

import time
from report import report_sxw
import tools
import xml
from lxml import etree

class report_fr_rml_parse(report_sxw.rml_parse):
        def formate(float=0):
            s=format('%.2f',float,True)

        def _add_header(self, rml_dom, header=1):
            nom=self.__class__.__name__
	    print nom
	    try:
                rml_head = tools.file_open('custom/'+nom+'.header.rml',subdir='addons/report_account_fr/report').read()
            except:
                if header==2:
                    rml_head =  self.rml_header2
                else:
                    rml_head =  self.rml_header
            if self.logo and (rml_head.find('company.logo')<0 or rml_head.find('<image')<0) and rml_head.find('<!--image')<0:
                rml_head =  rml_head.replace('<pageGraphics>','''<pageGraphics> <image x="10" y="26cm" height="70" width="90" >[[company.logo]] </image> ''')
            if not self.logo and rml_head.find('company.logo')>=0:
                rml_head = rml_head.replace('<image','<!--image')
                rml_head = rml_head.replace('</image>','</image-->')
            head_dom = etree.XML(rml_head)
            for tag in head_dom.getchildren():
                found = rml_dom.find('.//'+tag.tag)
                if found is not None and len(found):
                    if tag.get('position'):
                        found.append(tag)
                    else :
                        found.getparent().replace(found,tag)
            return True

                
               
