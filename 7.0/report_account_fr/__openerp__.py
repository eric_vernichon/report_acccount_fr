{
	"name" : "Account FR  Report",
	"version" : "0.9.6",
	"depends" : ["account", "base"],
	"author" : "Eric Vernichon",
	"description": """Etat comptables 	""",
	"category" : "Generic Modules/Accounting",
	"data" : [  "report.account.fr.parametres.csv",
	
		"report_account_fr_report.xml",
		"report_account_fr_view.xml",
        "security/ir.model.access.csv"
	],
	"active": False,
	"installable": True
}
